===================
Data Request Broker
===================
---------------------------------
SWIFT driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-swift/month
    :target: https://pepy.tech/project/drb-driver-swift
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-swift.svg
    :target: https://pypi.org/project/drb-driver-swift/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-swift.svg
    :target: https://pypi.org/project/drb-driver-swift/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-swift.svg
    :target: https://pypi.org/project/drb-driver-swift/
    :alt: Python Version Support Badge

-------------------


User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

Others
======
.. toctree::
   :maxdepth: 2

   user/limitation
