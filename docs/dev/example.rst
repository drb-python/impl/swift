.. _example:

Example
=======

Connect to your server
----------------------
.. literalinclude:: example/connect.py
    :language: python

Download a file
----------------
To avoid unnecessary downloads, the ``chunk_size`` parameter can be set
in the ``get_impl`` method. Especially if the default value (12000) is larger
than the read size.

.. literalinclude:: example/download.py
    :language: python

Get the attributes of a file
-----------------------------

.. literalinclude:: example/attributes.py
    :language: python

List container and Object
---------------------------
.. literalinclude:: example/children.py
    :language: python