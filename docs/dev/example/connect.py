from drb.drivers.swift import SwiftService, SwiftAuth

_os_options = {
    'user_domain_name': 'Default',
    'project_domain_name': 'Default',
    'project_name': 'project_name',
    'project_id': 'project_id',
    'tenant_name': 'tenant_name',
    'tenant_id': 'tenant_id',
    'region_name': 'region_name'
}

auth = SwiftAuth(authurl="https://your_auth_url/v3",
                 auth_version=3, tenant_name="tenant_name",
                 user="user",
                 key='password', os_options=_os_options)

node = SwiftService(auth=auth)

