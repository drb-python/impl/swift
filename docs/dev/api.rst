.. _api:

Reference API
=============

This driver wraps openstack swift service in drb node.
It provides a set of nodes representing the service it self (SwiftService),
the container representation (SwiftContainer),
and the object representation (SwiftObject).
All these representations follow DrbNode data model and their access
methods are optimized to speedup accesses to openstack service.



SwiftAuth
----------
.. autoclass:: drb.drivers.swift.SwiftAuth
    :members:

SwiftService
------------
.. autoclass:: drb.drivers.swift.SwiftService
    :members:

SwiftContainer
---------------
.. autoclass:: drb.drivers.swift.SwiftContainer
    :members:

SwiftObject
------------
.. autoclass:: drb.drivers.swift.SwiftObject
    :members: