.. _install:

Installation of swift driver
====================================
Installing ``drb-driver-swift`` with execute the following in a terminal:

.. code-block::

   pip install drb-driver-swift
