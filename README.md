# Swift Implementation
This drb-driver-swift module implements Swift protocol access with DRB data model.

## Swift Factory and Swift Node
The module implements the factory model defined in DRB in its node resolver. Based on the python entry point mechanism, this module can be dynamically imported into applications.

The entry point group reference is `drb.driver.swift`.<br/>
The implementation name is `swift`.<br/>
The factory class is encoded into `drb.driver.swift`.<br/>
The Swift signature id is  `86289118-7797-11ec-90d6-0242ac120003`<br/>


## Using this module
The project is present in https://www.pypi.org service. it can be freely 
loaded into projects with the following command line:

```commandline
pip install drb-driver-swift
```
## Access Data
`DrbSwiftNode` manages the swift protocol to access remote data. The construction
parameter is an authentication object.

```python
from drb.drivers.swift import SwiftService, SwiftAuth

_os_options = {
    'user_domain_name': 'Default',
    'project_domain_name': 'Default',
    'project_name': 'project_name',
    'project_id': 'project_id',
    'tenant_name': 'tenant_name',
    'tenant_id': 'tenant_id',
    'region_name': 'region_name'
}

auth = SwiftAuth(authurl="https://your_auth_url/v3",
                 auth_version=3, tenant_name="tenant_name",
                 user="user",
                 key='password', os_options=_os_options)

node = SwiftService(auth=auth)
```
When accessing a SwiftService the node gives access to all the container of this service by giving a list of ContainerNode,
and then each container gives a list of ObjectNode for each object in the container.

## Limitations

This implementation doesn't allow to write, modify, delete file on a swift container,
or it doesn't allow to delete or upload a file.
This implementation doesn't allow to download directly an all container.

## Documentation

The documentation of this implementation can be found here https://drb-python.gitlab.io/impl/swift