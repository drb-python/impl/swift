import os

import httpretty

global cpt
cpt = 0


def start_mock_swift(svc_url: str):
    resource_dir = os.path.join(os.path.dirname(__file__), 'resources')

    def attributes_callback(request, uri, headers):
        with open(os.path.join(resource_dir, 'attributes.json')) as f:
            data = bytes(f.read(), 'utf-8')

        return 200, headers, data

    cpt = 0

    def container_callback(request, uri, headers):
        with open(os.path.join(resource_dir, 'container.json')) as f:
            data = bytes(f.read(), 'utf-8')

        headers['Content-Type'] = 'swift/server'
        headers['swift'] = 'something'
        headers['swift-version'] = '4.0'
        headers['x-storage-url'] = 'https://my.storage/'

        return 200, headers, data

    def content_callback(request, uri, headers):
        global cpt
        if cpt == 0:
            with open(os.path.join(resource_dir,
                                   'container_content.json')) as f:
                data = bytes(f.read(), 'utf-8')
        else:
            data = bytes("[]", 'utf-8')
        cpt += 1
        return 200, headers, data

    def object_callback(request, uri, headers):
        data = bytes("This is my awesome test.", 'utf-8')
        return 200, headers, data

    def head_callback(request, uri, headers):
        return 200, {'swift': 'Whaou'}, b''

    httpretty.enable()
    httpretty.register_uri(httpretty.HEAD, f"{svc_url}",
                           head_callback)
    httpretty.register_uri(httpretty.GET, f"{svc_url}?format=json",
                           container_callback)
    httpretty.register_uri(httpretty.GET, f"{svc_url}info",
                           attributes_callback)
    httpretty.register_uri(httpretty.GET, f"{svc_url}/container-1?format=json",
                           content_callback)
    httpretty.register_uri(httpretty.GET, f"{svc_url}/container-1/test.txt",
                           object_callback)


def stop_mock_swift():
    httpretty.disable()
    httpretty.reset()


def reset_cpt():
    global cpt
    cpt = 0
